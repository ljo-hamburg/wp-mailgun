=== Mailgun for WordPress ===
Tags: mail, mailgun, forms, template
Requires at least: 4.6
Requires PHP: 7.4
License: MIT
License URI: https://opensource.org/licenses/MIT

A WordPress plugin that adds various Mailgun features.

== Description ==
This plugin contains:
* A NinjaForm Add-On
* A Mailing List Subscription Form

== Installation ==
Install the plugin as usual.

=== Ninja Forms Add-On ===
Go to Ninja Forms > Settings and enter your Mailgun domain and API key. If you setup your domain on the EU servers also
check the \"Use EU Server\" box. Now you can add the Mailgun action to your forms.

== Changelog ==
= 1.0.0 =
* First Release
