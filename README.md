# NinjaForms Mailgun Action

This is a WordPress plugin that adds a Mailgun action to Ninja Forms allowing you to use Mailgun templates in Ninja Forms.

## Installation

To use your Mailgun templates with NinjaForms do the following steps:

1. Install and activate the plugin
2. Go to Ninja Forms → Settings
3. Enter your Mailgun domain and API key in the *Mailgun* section. If you are using the EU servers, check the *Use EU Server* box.

## Usage

Using Mailgun templates as Ninja Forms actions is easy. To use them in a form open up the form and go to its *E-Mails and Actions*. Here you can add the **Mailgun Template** action. The configuration for the tempalte is similar to the default E-Mail action. You can configure recipients the sender as well as other headers. The most notable difference is the *Template Name* field which you can use to specify the name of the Mailgun template that should be used.

By default all form field values are sent when the action is fired. This way any value that the user entered is available to the template. If you need to interpolate values in Ninja Forms or want to include any custom data you can use the *Custom JSON Data* field in the advanced settings of the action. Please note that the value for this field needs to be a [valid JSON](https://jsonlint.com) encoded string.