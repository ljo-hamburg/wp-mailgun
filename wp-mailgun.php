<?php
/**
 * Mailgun for WordPress
 *
 * @author  undefined
 * @license MIT
 * @link    https://gitlab.com/ljo-hamburg/wp-mailgun
 * @package wp-mailgun
 *
 * @wordpress-plugin
 * Plugin Name:       Mailgun for WordPress
 * Plugin URI:        https://gitlab.com/ljo-hamburg/wp-mailgun
 * Description:       A WordPress plugin for Mailgun.
 * Version:           1.0.0
 * Author:            Kim Wittenburg
 * Author URI:        https://ljo-hamburg.de
 * License:           MIT
 * Text Domain:       wp-mailgun
 * GitLab Plugin URI: https://gitlab.com/ljo-hamburg/wp-mailgun
 * Release Asset:     true
 * GitLab CI Job:     release
 */

if ( ! defined( 'WPINC' ) ) {
    die;
}

define( 'WP_MAILGUN_VERSION', '1.0.0' );
?>
