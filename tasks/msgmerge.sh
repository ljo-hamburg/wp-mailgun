#!/usr/bin/env sh

for file in languages/*.po; do
  filename=$(basename "$file")
  potName=wp-mailgun${filename#wp-mailgun-??_??}t
  echo $potName
  msgmerge -U "$file" languages/$potName
done
