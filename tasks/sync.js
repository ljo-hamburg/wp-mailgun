#!/usr/bin/env node

/**
 * This script synchronizes entries from package.json to composer.json and generates the style.css file.
 */

const fs = require("fs");

const pkg = require('../package.json');

// Update composer.json
const composer = require('../composer.json');
composer._comment = "The content of this file is updated automatically based on package.json. Use the " +
    "tasks/sync.js script to sync changes from package.json to this file.";
composer.name = "ljo-hamburg/" + pkg.name;
composer.version = pkg.version;
composer.description = pkg.description;
composer.license = pkg.license;
author = pkg.author.match(/^(?<name>.+?)\s*(<(?<email>.+?)>)?\s*(\((?<homepage>.+?)\))?$/).groups;
composer.authors = [author];
fs.writeFileSync('composer.json', JSON.stringify(composer, null, 2));

// Generate style.css
fs.writeFileSync('wp-mailgun.php', `<?php
/**
 * ${pkg.wpData.pluginName}
 *
 * @author  ${pkg.author.name}
 * @license ${pkg.license}
 * @link    ${pkg.wpData.gitLabPluginURI}
 * @package ${pkg.name}
 *
 * @wordpress-plugin
 * Plugin Name:       ${pkg.wpData.pluginName}
 * Plugin URI:        ${pkg.wpData.gitLabPluginURI}
 * Description:       ${pkg.wpData.description}
 * Version:           ${pkg.version}
 * Author:            ${author.name}
 * Author URI:        ${author.homepage}
 * License:           ${pkg.license}
 * Text Domain:       ${pkg.name}
 * GitLab Plugin URI: ${pkg.wpData.gitLabPluginURI}
 * Release Asset:     ${pkg.wpData.releaseAsset}
 * GitLab CI Job:     ${pkg.wpData.gitLabCIJob}
 */

if ( ! defined( 'WPINC' ) ) {
    die;
}

define( 'WP_MAILGUN_VERSION', '${pkg.version}' );
?>
`);


