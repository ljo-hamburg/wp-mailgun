#!/usr/bin/env sh

# === Sync Changes from package.json ===
"$(dirname $0)/sync.js"

# === "Compile" (aka Copy) PHP Code ===
mkdir -p build/
# Install Dependencies
sed 's|./../|./../../|g' composer.json >build/composer.json
COMPOSER_MIRROR_PATH_REPOS=1 composer --no-interaction -dbuild install --no-dev

# Copy Code
cp -Rf src/php/ build/
# Create Main Plugin File
cat wp-mailgun.php src/php/wp-mailgun.php >build/wp-mailgun.php

# === Build Assets ===
# Copy Static Assets
cp -Rf src/img/ build/assets/
# JS and CSS
webpack
# Plugin Icon
cp src/icon.svg build/assets/
cat src/icon.svg | convert-svg-to-png --width=128 --height=128 --filename build/assets/icon-128x128.png
cat src/icon.svg | convert-svg-to-png --width=256 --height=256 --filename build/assets/icon-256x256.png

# === Build Translations ===
mkdir -p build/languages
for file in languages/*.po; do
  filename=$(basename -- "$file")
  name="${filename%.po}"
  msgfmt -o "build/languages/$name.mo" "$file"
done
wp i18n make-json --no-purge languages/ build/languages/

# === Copy Static Files ===
cp LICENSE README.txt build/
