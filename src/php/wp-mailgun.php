<?php

// Check if composer helpers are available
use Codello\WPMailgun\MailgunAction;
use Codello\WPMailgun\MailgunPlugin;
use Codello\WPMailgun\MailgunSettings;

if ( ! @include plugin_dir_path( __FILE__ ) . 'vendor/autoload.php' ) {
	deactivate_plugins( plugin_basename( __FILE__ ) );
	wp_die( __( 'This plugin is not completely installed. This issue may occur if you installed the plugin without the 
    required dependencies. Please reinstall or update the plugin.', 'wp-mailgun' ),
		'Plugin not completely installed',
		[ 'back_link' => true ] );
}


// Register plugin wide settings
require_once __DIR__ . '/classes/MailgunPlugin.php';
$plugin = new MailgunPlugin( __FILE__ );
$plugin->register();

if ( MailgunSettings::validate() ) {
	// Register Ninja Forms Action
	add_filter( 'ninja_forms_register_actions', function ( $types ) {
		require_once plugin_dir_path( __FILE__ ) . 'ninja-forms/MailgunAction.php';

		return $types + [
				"wp-mailgun-ninja-forms" => new MailgunAction(),
			];
	} );
}