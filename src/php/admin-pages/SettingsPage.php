<?php

namespace Codello\WPMailgun;

require_once "DomainSettingsField.php";
require_once "MailingListSettingsField.php";

use Codello\WP\NamedEmailSettingsField;
use Codello\WP\SelectPageSettingsField;
use Codello\WP\SelectRolesSettingsField;
use Codello\WP\SelectSettingsField;
use Codello\WP\SettingsSection;
use Codello\WP\TextSettingsField;

class SettingsPage extends \Codello\WP\SettingsPage {

	public function __construct() {
		parent::__construct( 'wp-mailgun' );
	}

	public function setup() {
		$this->init( __( 'Mailgun Settings', 'wp-mailgun' ), __( 'Mailgun', 'wp-mailgun' ) );
	}

	public function addSections() {
		$generalSection = ( new SettingsSection( 'wp-mailgun-general-section', $this ) )
			->init( __( 'Mailgun Account', 'wp-mailgun' ) );
		( new TextSettingsField( 'wp-mailgun-api-key', $generalSection ) )->init( __( 'API Key', 'wp-mailgun' ), [
			"placeholder" => 'key-…',
			'required'    => true,
			"cssClasses"  => [ 'regular-text', 'code' ],
		] );
		( new SelectSettingsField( 'wp-mailgun-server', $generalSection ) )->init( __( 'Server Location', 'wp-mailgun' ), [
			"options" => [
				"us" => __( 'United States', 'wp-mailgun' ),
				"eu" => __( 'Europe', 'wp-mailgun' ),
			],
			"default" => "eu",
		] );
		( new DomainSettingsField( 'wp-mailgun-domain', $generalSection ) )->init( __( 'Domain', 'wp-mailgun' ) );
		( new SelectRolesSettingsField( 'wp-mailgun-capability', $generalSection ) )->init( __( 'Required Role', 'wp-mailgun' ), [
			"default"     => "administrator",
			"description" => _( 'Select the role required to edit newsletter forms and invite users.', 'wp-mailgun' ),
		] );
	}
}