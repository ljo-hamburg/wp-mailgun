<?php

namespace Codello\WPMailgun;

use Codello\WP\AdminPage;
use Codello\WP\NamedEmailSettingsField;
use Codello\WP\SelectPageSettingsField;
use Codello\WP\TextAreaSettingsField;
use Codello\WP\TextSettingsField;
use Codello\WP\WPScript;
use Codello\WP\WPStyle;
use Mailgun\Model\MailingList\MailingList;

class NewsletterPage extends AdminPage {

	private WPScript $script;

	public function __construct() {
		parent::__construct( 'wp-mailgun-newsletter' );
	}

	public function getCapability(): string {
		return MailgunSettings::getCapability();
	}

	public function getParentPage(): ?AdminPage {
		return AdminPage::get( 'tools.php' );
	}

	public function getPosition(): ?int {
		return 3;
	}

	public function register() {
		parent::register();
		( new WPStyle( 'wp-mailgun-newsletter-tools', plugins_url( 'assets/newsletter-tools.css', MailgunPlugin::instance()->getFile() ) ) )->register();
		$this->script = new WPScript( 'wp-mailgun-newsletter-tools',
			plugins_url( 'assets/newsletter-tools.js', MailgunPlugin::instance()->getFile() ),
			[ 'jquery', 'wp-i18n', ],
			true, [
				"footer"          => true,
				"textDomain"      => "wp-mailgun",
				"translationPath" => MailgunPlugin::instance()->getTranslationPath(),
				"autoEnqueue"     => false,
			] );
		$this->script->register();
	}

	public function setup() {
		$this->init( __( 'Newsletter' ), __( 'Mailgun Newsletter' ) );
	}

	public function render() {
		$this->script->enqueueScript();
		try {
			$lists = MailgunPlugin::mailgun()->mailingList()->pages()->getLists();
			$error = null;
		} catch ( \Exception $e ) {
			$lists = [];
			$error = $e;
		}
		?>

        <div class="wrap">
            <h2><?= esc_html__( $this->getTitle() ) ?></h2>
			<?= esc_html__( "The tools on this page allow you to configure advanced settings for your newsletters.", "wp-mailgun" ) ?>
			<?php if ( $error )
				return $this->renderError( $error ) ?>
			<?php $this->renderMailingListsSection( $lists ) ?>
			<?php $this->renderInviteSection( $lists ) ?>
        </div>
	<?php }

	private function renderError( $error ) { ?>
        <div class="notice notice-error">
            <p><?= esc_html__( "Could not list your mailing lists. Check if your API key is valid.", "wp-mailgun" ) ?></p>
        </div>
	<?php }

	/**
	 * @param MailingList[] $lists
	 */
	private function renderMailingListsSection( array $lists ) { ?>
        <h2><?= esc_html__( 'Customize Mailing Lists', 'wp-mailgun' ) ?></h2>
        <p><?= esc_html__( "Here you can customize the subscription forms for your mailing lists.", 'wp-mailgun' ) ?></p>
		<?php if ( empty( $lists ) ): ?>
            <p class="error-color"><?= esc_html( "Could not find any mailing lists", 'wp-mailgun' ) ?></p>
		<?php else: ?>
            <form id="mailing-list-edit-form" class="modal" action="<?= admin_url( 'admin-ajax.php' ) ?>" method="post">
                <h1 class="name"></h1>
                <p><?= esc_html__( "Configure the settings for this mailing list. This does not affect any other mailing lists.", "wp-mailgun" ) ?></p>
                <p class="error-message"></p>
                <input type="hidden" name="action" value="wp-mailgun-update-mailing-list">
                <input type="hidden" name="list">
				<?php wp_nonce_field( 'wp-mailgun-update-mailing-list' ) ?>
                <div class="form-field">
                    <label for="wp-mailgun-list-edit-subject"><?= esc_html__( 'Subject', 'wp-mailgun' ) ?></label>
					<?php TextSettingsField::renderStandalone( 'wp-mailgun-list-edit-subject', 'subject', null, [
						"required" => true,
					] ) ?>
                </div>
                <div class="form-field">
                    <label for="wp-mailgun-list-edit-sender"><?= esc_html__( 'Sender Email', 'wp-mailgun' ) ?></label>
					<?php NamedEmailSettingsField::renderStandalone( 'wp-mailgun-list-edit-sender', 'sender', null, [
						"required"    => true,
						"description" => __( "Use the format 'Name <email@example.tld>' to include a sender name.", "wp-mailgun" ),
					] ) ?>
                </div>
                <div class="form-field">
                    <label for="wp-mailgun-list-edit-template"><?= esc_html__( 'Template', 'wp-mailgun' ) ?></label>
					<?php TextSettingsField::renderStandalone( 'wp-mailgun-list-edit-template', 'template', null, [
						"required"    => true,
						// TODO: Document available variables.
						"description" => __( "This template is used for the double opt-in confirmation mail..", "wp-mailgun" ),
					] ) ?>
                </div>
                <div class="form-field">
                    <label for="wp-mailgun-list-edit-page"><?= esc_html__( 'Successful Subscription Page', 'wp-mailgun' ) ?></label>
					<?php SelectPageSettingsField::renderStandalone( 'wp-mailgun-list-edit-page', 'successPage', null, [
						"empty"       => "---------------",
						"description" => __( "Select the page to which the user should be redirected after a successful subscription.", "wp-mailgun" ),
					] ) ?>
                </div>
                <div class="form-field">
                    <label for="wp-mailgun-list-edit-success-message"><?= esc_html__( 'Success Message', 'wp-mailgun' ) ?></label>
					<?php TextAreaSettingsField::renderStandalone( 'wp-mailgun-list-edit-success-message', 'successMessage', null, [
						"description" => __( "This message is displayed when a user successfully submitted the subscribe form.", "wp-mailgun" ),
					] ) ?>
                </div>
                <div class="form-field">
                    <label for="wp-mailgun-list-edit-error-message"><?= esc_html__( 'Success Message', 'wp-mailgun' ) ?></label>
					<?php TextAreaSettingsField::renderStandalone( 'wp-mailgun-list-edit-error-message', 'errorMessage', null, [
						"description" => __( "This message is displayed when a user did enter invalid data in the subscribe form.", "wp-mailgun" ),
					] ) ?>
                </div>

                <div class="buttons">
                    <a class="button cancel" type="button" href="#"
                       rel="modal:close"><?= esc_html__( 'Cancel', 'wp-mailgun' ) ?></a>
                    <button class="button button-primary submit"
                            type="submit"><?= esc_html__( 'Save', 'wp-mailgun' ) ?></button>
                </div>
            </form>
            <table class="mg-mailing-lists widefat striped">
                <tbody>
				<?php foreach ( $lists as $list ): ?>
					<?php $settings = MailgunSettings::getMailingListSettings( $list->getAddress() ) ?>
                    <tr class="mg-mailing-list"
                        data-list-name="<?= esc_attr( $list->getName() ) ?>"
                        data-list-address="<?= esc_attr( $list->getAddress() ) ?>"
                        data-subject="<?= esc_attr( $settings->subject ) ?>"
                        data-sender="<?= esc_attr( $settings->sender ) ?>"
                        data-template="<?= esc_attr( $settings->template ) ?>"
                        data-success-page="<?= esc_attr( $settings->successPage ) ?>"
                        data-success-message="<?= esc_attr( $settings->successMessage ) ?>"
                        data-error-message="<?= esc_attr( $settings->errorMessage ) ?>">
                        <td>
                            <span class="dot <?= $settings->isValid() ? 'green' : 'red' ?>"></span>
                        </td>
                        <td class="title">
                            <div class="name"><?= esc_html( $list->getName() ) ?></div>
                            <div class="description"><?= esc_html( sprintf( __( '%1$s (%2$s Members)', 'wp-mailgun' ), $list->getAddress(), $list->getMembersCount() ) ) ?></div>
                        </td>
                        <td><?= esc_html( $list->getDescription() ) ?></td>
                        <td><a class="configure button" href="#"><?= esc_html( 'Configure', 'wp-mailgun' ) ?></a></td>
                    </tr>
				<?php endforeach ?>
                </tbody>
            </table>
		<?php endif ?>
	<?php }

	/**
	 * @param MailingList[] $lists
	 */
	private function renderInviteSection( array $lists ) { ?>
        <h2><?= esc_html__( 'Send Manual Invites', 'wp-mailgun' ) ?></h2>
        <p><?= esc_html__( "Invite users to join your mailing list. Users will receive an email and can click a
             link in that email to subscribe to your newsletter. You can use {{ newsletter_subscribe_link }} in your
             email to include that subscribe link.", 'wp-mailgun' ) ?></p>
        <form action="<?= admin_url( 'admin-ajax.php' ) ?>" method="post"
              class="newsletter-invite wp-mailgun-admin-section">
			<?php wp_nonce_field( 'wp-mailgun-send-invitations' ) ?>
            <input type="hidden" name="action" value="wp-mailgun-send-invitations">
            <div class="form-section">
                <div class="form-field" style="flex:0.5">
                    <label for="wp-mailgun-invite-list"><?= esc_html__( 'Mailing List', 'wp-mailgun' ) ?></label>
                    <select name="list" id="wp-mailgun-invite-list">
						<?php foreach ( $lists as $list ): ?>
                            <option value="<?= esc_attr( $list->getAddress() ) ?>"><?= esc_html( $list->getName() ) ?></option>
						<?php endforeach ?>
                    </select>
                </div>
                <div class="form-field">
                    <label for="wp-mailgun-invite-template"><?= esc_html__( 'Template', 'wp-mailgun' ) ?></label>
                    <input type="text" name="template" id="wp-mailgun-invite-template" class="regular-text" required>
                </div>
            </div>
            <div class="form-field">
                <label for="wp-mailgun-invite-subject"><?= esc_html__( 'Subject', 'wp-mailgun' ) ?></label>
                <input type="text" name="subject" id="wp-mailgun-invite-subject" class="regular-text" required>
            </div>
            <div class="form-field">
                <label for="wp-mailgun-invite-addresses"><?= esc_html__( 'Addresses', 'wp-mailgun' ) ?></label>
                <textarea class="large-text code"
                          name="addresses"
                          id="wp-mailgun-invite-addresses"
                          required></textarea>
                <p class="help">
					<?= esc_html__( "Enter the recipients' email addresses in the format
                         'Name <address@example.tld>'. Multiple addresses can be separated by a comma.", 'wp-mailgun' ) ?>
                </p>
            </div>
            <button class="button button-primary" type="submit"><?= esc_html__( 'Invite', 'wp-mailgun' ) ?></button>
        </form>
	<?php }
}
