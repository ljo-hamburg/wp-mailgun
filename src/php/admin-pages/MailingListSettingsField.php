<?php

namespace Codello\WPMailgun;

use Codello\WP\SelectSettingsField;

class MailingListSettingsField extends SelectSettingsField {

	public function init( string $label = "", array $args = [] ): self {
		$this->selectionRequest = __( 'Please Choose', 'wp-mailgun' );
		parent::init( $label, $args );

		return $this;
	}

	public function getConfigurationError(): ?string {
		$mailgun = MailgunPlugin::mailgun();
		if ( ! isset( $mailgun ) ) {
			return __( 'Enter your API key and save to select a mailing list.', 'wp-mailgun' );
		} else if ( empty( $this->getOptions() ) ) {
			return __( "You don't have any mailing lists yet.", 'wp-mailgun' );
		}

		return parent::getConfigurationError();
	}

	public function makeOptions(): array {
		$lists = [];
		try {
			$mgLists = MailgunPlugin::mailgun()->mailingList()->pages()->getLists();
			foreach ( $mgLists as $list ) {
				$lists[ $list->getAddress() ] = sprintf( __( "%s (%d Members)", 'wp-mailgun' ), $list->getName(), $list->getMembersCount() );
			}
		} catch ( \Exception $e ) {
			$lists = [];
		}

		return $lists;
	}

	public function getErrorMessages(): array {
		return array_merge( parent::getErrorMessages(), [
			'option-not-found' => __( 'The specified mailing list does not exist', 'wp-mailgun' ),
		] );
	}
}