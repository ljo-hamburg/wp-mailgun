<?php

namespace Codello\WPMailgun;

use Codello\WP\SelectSettingsField;

class DomainSettingsField extends SelectSettingsField {

	public function init( string $label = "", array $args = [] ): SelectSettingsField {
		$this->selectionRequest = __( 'Please Choose', 'wp-mailgun' );

		return parent::init( $label, $args );
	}

	public function getConfigurationError(): ?string {
		$mailgun = MailgunPlugin::mailgun();
		if ( ! isset( $mailgun ) ) {
			return __( 'Enter your API key and save to select a domain.', 'wp-mailgun' );
		} else if ( empty( $this->getOptions() ) ) {
			return __( "No domains found in your account" );
		} else {
			return parent::getConfigurationError();
		}
	}

	public function makeOptions(): array {
		$domains = [];
		try {
			$mgDomains = MailgunPlugin::mailgun()->domains()->index()->getDomains();
			foreach ( $mgDomains as $domain ) {
				$domains[ $domain->getName() ] = $domain->getName();
			}
		} catch ( \Exception $e ) {
			$domains = [];
		}

		return $domains;
	}

	public function getErrorMessages(): array {
		return array_merge( parent::getErrorMessages(), [
			'option-not-found' => __( "The Mailgun domain was not found.", 'wp-mailgun' ),
		] );
	}

	/*public function sanitize( $value ) {
		$value = parent::sanitize( $value );
		try {
			MailgunPlugin::mailgun()->domains()->show( $value );

			return $value;
		} catch ( Exception $e ) {
			$this->addError( 'invalid-domain' );

			return "";
		}
	}*/
}