<?php

namespace Codello\WPMailgun;

use Codello\WP\PluginActionLink;
use Codello\WP\WPPlugin;
use Http\Client\Common\PluginClient;
use Mailgun\HttpClient\HttpClientConfigurator;
use Mailgun\HttpClient\RequestBuilder;
use Mailgun\Hydrator\Hydrator;
use Mailgun\Hydrator\ModelHydrator;
use Mailgun\Mailgun;

require_once __DIR__ . "/../actions/SubscribeAction.php";
require_once __DIR__ . "/../actions/SubscriptionRequestAction.php";
require_once __DIR__ . "/../actions/InvitationAction.php";
require_once __DIR__ . "/../actions/MailingListUpdateAction.php";
require_once "MailgunSettings.php";
require_once __DIR__ . "/../admin-pages/SettingsPage.php";
require_once __DIR__ . "/../admin-pages/NewsletterPage.php";
require_once __DIR__ . '/../shortcodes/MailgunShortcode.php';

// TODO: Possibly support overriding wp_mail
// TODO: Possibly support link/open tracking
class MailgunPlugin extends WPPlugin {

	private SubscribeAction $subscribeAction;

	public function __construct( string $file ) {
		parent::__construct( $file );
		$this->subscribeAction = new SubscribeAction();
	}

	public static function mailgun() {
		return self::instance()->getMailgun();
	}

	public function getSlug(): string {
		return "wp-mailgun";
	}

	public function register() {
		parent::register();
		$settingsPage = new SettingsPage();
		$settingsPage->register();
		( PluginActionLink::createSettingsLink( $this, $settingsPage ) )->register();
		if ( MailgunSettings::validate() ) {
			$this->subscribeAction->register();
			( new SubscriptionRequestAction() )->register();
			( new MailgunShortcode() )->register();
			( new NewsletterPage() )->register();
			( new InvitationAction() )->register();
			( new MailingListUpdateAction() )->register();
		}
	}

	public function validateConfig() {
		$errors = parent::getConfigErrors();
		if ( ! MailgunSettings::validate() ) {
			$errors[] = __( 'Your Mailgun Settings are incomplete', 'wp-mailgun' );
		}

		return $errors;
	}

	private $mailgun = null;
	private $httpClientConfigurator = null;
	private $httpClient = null;
	private $hydrator = null;
	private $requestBuilder = null;

	public function getHttpClientConfigurator(): HttpClientConfigurator {
		if ( ! MailgunSettings::validate() ) {
			return null;
		}
		if ( ! isset( $this->httpClientConfigurator ) ) {
			$this->httpClientConfigurator = ( new HttpClientConfigurator() )
				->setApiKey( MailgunSettings::getAPIKey() )
				->setEndpoint( MailgunSettings::getMailgunEndpoint() );
		}

		return $this->httpClientConfigurator;
	}

	public function getHydrator(): Hydrator {
		if ( ! $this->hydrator ) {
			$this->hydrator = new ModelHydrator();
		}

		return $this->hydrator;
	}

	public function getRequestBuilder(): RequestBuilder {
		if ( ! $this->requestBuilder ) {
			$this->requestBuilder = new RequestBuilder();
		}

		return $this->requestBuilder;
	}

	public function getHttpClient(): PluginClient {
		if ( ! MailgunSettings::validate() ) {
			return null;
		}
		if ( ! $this->httpClient ) {
			$this->httpClient = $this->getHttpClientConfigurator()->createConfiguredClient();
		}

		return $this->httpClient;
	}

	public function getMailgun(): Mailgun {
		if ( ! MailgunSettings::validate() ) {
			return null;
		}
		if ( ! isset( $this->mailgun ) ) {
			$this->mailgun = new Mailgun( $this->getHttpClientConfigurator(), $this->getHydrator(), $this->getRequestBuilder() );
		}

		return $this->mailgun;
	}

	public function getSubscribeLink( string $name, string $address, string $list ): string {
		return admin_url( 'admin-ajax.php' ) . '?action=' . $this->subscribeAction->getName()
		       . '&name=' . urlencode( $name )
		       . '&address=' . urlencode( $address )
		       . '&list=' . urlencode( $list )
		       . '&key=' . urlencode( $this->generateConfirmationHash( $name, $address, $list ) );
	}

	/** @noinspection PhpUnusedParameterInspection */
	public function generateConfirmationHash( string $name, string $address, string $list ): string {
		return wp_hash( $address . '|' . $list );
	}

	public function validateConfirmationHash( string $name, string $address, string $list, string $hash ): bool {
		return $this->generateConfirmationHash( $name, $address, $list ) === $hash;
	}

}