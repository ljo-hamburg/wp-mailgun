<?php

namespace Codello\WPMailgun;

use Mailgun\Custom\Templates;

final class MailgunSettings {

	// TODO: Add Function to delete all settings and call that when uninstalling the plugin

	private function __construct() {
	}

	public static function validate() {
		return self::getAPIKey() && self::getMailgunDomain();
	}

	public static function getAPIKey() {
		return get_option( 'wp-mailgun-api-key' );
	}

	public static function getMailgunDomain() {
		return get_option( 'wp-mailgun-domain' );
	}

	public static function getCapability() {
		$capability = get_option( 'wp-mailgun-capability' );
		if ( ! $capability ) {
			return "administrator";
		}

		return $capability;
	}

	public static function setMailingListSettings( string $address, MailingListSettings $settings ) {
		$allSettings             = get_option( 'wp-mailgun-mailing-lists', [] );
		$allSettings[ $address ] = $settings->getData();
		update_option( 'wp-mailgun-mailing-lists', $allSettings );
	}

	public static function getMailingListSettings( $address ): MailingListSettings {
		$settings = get_option( 'wp-mailgun-mailing-lists', [] );
		if ( array_key_exists( $address, $settings ) ) {
			return new MailingListSettings( $address, $settings[ $address ] );
		} else {
			return new MailingListSettings( $address );
		}
	}

	public static function getMailgunEndpoint() {
		return ( get_option( 'wp-mailgun-server' ) == "eu" ) ? "https://api.eu.mailgun.net/" : "https://api.mailgun.net/";
	}
}

class MailingListSettings {
	private string $address;
	public string $subject;
	public string $sender;
	public string $template;
	public ?int $successPage;

	public string $successMessage;
	public string $errorMessage;

	public function __construct( string $address, array $data = [] ) {
		$this->address = $address;
		$this->load( $data );
	}

	private function load( array $data ) {
		$this->loadKey( 'subject', $data, "" );
		$this->loadKey( 'sender', $data, "" );
		$this->loadKey( 'template', $data, "" );
		$this->loadKey( 'successPage', $data, null );
		$this->loadKey( 'successMessage', $data, __( "A confirmation mail was sent to you. Please click the link in the activation mail in order to complete your subscription." ) );
		$this->loadKey( 'errorMessage', $data, __( "An error occurred. Please check your entries and try again." ) );
	}

	private function loadKey( $key, $data, $default ) {
		$value = $default;
		if ( array_key_exists( $key, $data ) ) {
			$dataValue = $data[ $key ];
			if ( $dataValue ) {
				$value = $dataValue;
			}
		}
		$this->$key = $value;
	}

	public function getData(): array {
		return [
			"subject"        => $this->subject,
			"sender"         => $this->sender,
			"template"       => $this->template,
			"successPage"    => $this->successPage,
			"successMessage" => $this->successMessage,
			"errorMessage"   => $this->errorMessage,
		];
	}

	public function isValid() {
		if ( empty( $this->sender ) || empty( $this->subject ) ) {
			return false;
		}
		if ( $this->successPage && ( get_post_type( $this->successPage ) != 'page' ) ) {
			return false;
		}
		try {
			( new Templates( MailgunPlugin::instance()->getHttpClient(),
				MailgunPlugin::instance()->getRequestBuilder(),
				MailgunPlugin::instance()->getHydrator() ) )->show( MailgunSettings::getMailgunDomain(), $this->template );
		} catch ( \Exception $e ) {
			return false;
		}

		return true;
	}

	public function getSuccessURL(): ?string {
		if ( $this->successPage ) {
			return get_permalink( $this->successPage );
		} else {
			return null;
		}
	}

	public function save() {
		MailgunSettings::setMailingListSettings( $this->address, $this );
		$this->load( $this->getData() );
	}

}

