<?php

namespace Codello\WPMailgun;

require_once __DIR__ . '/../mailgun/templates/Templates.php';
require_once __DIR__ . "/../mailgun/CustomBatchMessage.php";

use Codello\WP\AjaxAction;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Egulias\EmailValidator\Validation\MultipleValidationWithAnd;
use Egulias\EmailValidator\Validation\RFCValidation;
use Mailgun\Custom\CustomBatchMessage;
use Mailgun\Custom\MailingListMember;
use Mailgun\Custom\Templates;
use Mailgun\Message\Exceptions\MissingRequiredParameter;
use Mailgun\Message\Exceptions\TooManyRecipients;

class InvitationAction extends AjaxAction {

	public function __construct() {
		parent::__construct( 'wp-mailgun-send-invitations' );
	}

	public function perform(): void {
		$template  = $this->getParameter( "template", true );
		$subject   = $this->getParameter( 'subject', true );
		$addresses = $this->getParameter( "addresses", true );
		$list      = $this->getParameter( 'list', true );
		$addresses = $this->validate( $template, $list, $addresses );
		$this->checkForErrors();
		$mailingList = MailgunSettings::getMailingListSettings( $list );

		$mailgunPlugin = MailgunPlugin::instance();
		$batchMessage  = new CustomBatchMessage( MailgunPlugin::mailgun()->messages(), MailgunSettings::getMailgunDomain(), true );
		$batchMessage->setFromAddress( $mailingList->sender );
		$batchMessage->setSubject( $subject );
		$batchMessage->addCustomParameter( 'template', $template );
		$batchMessage->addCustomParameter( 't:text', "yes" );
		$batchMessage->addCustomData( 'subject', $subject );

		try {
			foreach ( $addresses as $address ) {
				$name    = $address['display'];
				$email   = $address['address'];
				$options = [
					"subscribe_link" => MailgunPlugin::instance()->getSubscribeLink( $name, $email, $list ),
				];
				if ( $name == $email ) {
					$address = $email;
					$name    = '';
				} else {
					$address = $name . ' <' . $email . '>';
				}
				$batchMessage->addToRecipient( $address, $options );
				( new MailingListMember( $mailgunPlugin->getHttpClient(),
					$mailgunPlugin->getRequestBuilder(),
					$mailgunPlugin->getHydrator() ) )
					->create( $list, $address, $name, [], true, true );
				MailgunPlugin::mailgun()->mailingList()->member()->create( $list, $email, $name, [], false );
			}
			$batchMessage->finalize();
		} catch ( MissingRequiredParameter $e ) {
			$this->addError( 'no-new-addresses', '', 400 );
		} catch ( TooManyRecipients $e ) {
			$this->addError( 'too-many-recipients', '', 400 );
		} catch ( \Exception $e ) {
			var_dump( $e );
			$this->addError( 'unknown', $e );
		}
		$this->done( "Done" );
	}

	private function validate( $template, $list, $addresses ) {
		$this->validateNonce();
		$this->checkForErrors();
		try {
			MailgunPlugin::mailgun()->mailingList()->show( $list );
		} catch ( \Exception $e ) {
			$this->addError( 'invalid-mailing-list', $list );
		}
		$addresses = mailparse_rfc822_parse_addresses( $addresses );
		$sanitized = [];
		$validator = new EmailValidator();
		foreach ( $addresses as $address ) {
			$email = $address['address'];
			$valid = $validator->isValid( $email, new MultipleValidationWithAnd( [
				new RFCValidation(),
				new DNSCheckValidation(),
			] ) );
			if ( ! $valid ) {
				$this->addError( 'invalid-email', $email, 400 );
			} else {
				try {
					MailgunPlugin::mailgun()->mailingList()->member()->show( $list, $email );
				} catch ( \Exception $e ) {
					$sanitized[] = $address;
				}
			}
		}
		$httpClient     = MailgunPlugin::instance()->getHttpClient();
		$requestBuilder = MailgunPlugin::instance()->getRequestBuilder();
		$hydrator       = MailgunPlugin::instance()->getHydrator();
		$templates      = new Templates( $httpClient, $requestBuilder, $hydrator );
		try {
			$templates->show( MailgunSettings::getMailgunDomain(), $template );
		} catch ( \Exception $e ) {
			$this->addError( 'template-not-found', $template, 400 );
		}

		return $sanitized;
	}
}