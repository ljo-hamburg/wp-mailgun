<?php


namespace Codello\WPMailgun;


use Codello\WP\AjaxAction;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\RFCValidation;
use Mailgun\Custom\Templates;

class MailingListUpdateAction extends AjaxAction {

	public function __construct() {
		parent::__construct( 'wp-mailgun-update-mailing-list', true );
	}

	public function perform(): void {
		$list           = $this->getParameter( 'list', true );
		$subject        = $this->getParameter( 'subject', true );
		$sender         = $this->getParameter( 'sender', true );
		$template       = $this->getParameter( 'template', true );
		$successMessage = $this->getParameter( 'successMessage' );
		$errorMessage   = $this->getParameter( 'errorMessage' );
		$successPage    = $this->getParameter( 'successPage' );
		$this->validate( $list, $subject, $sender, $template, $successPage );
		$this->checkForErrors();
		$settings                 = MailgunSettings::getMailingListSettings( $list );
		$settings->subject        = $subject;
		$settings->sender         = $sender;
		$settings->template       = $template;
		$settings->successMessage = $successMessage;
		$settings->errorMessage   = $errorMessage;
		$settings->successPage    = intval( $successPage );
		$settings->save();
		$this->done( [
			"list"           => $list,
			"subject"        => $settings->subject,
			"sender"         => $settings->sender,
			"template"       => $settings->template,
			"successPage"    => $settings->successPage,
			"successMessage" => $settings->successMessage,
			"errorMessage"   => $settings->errorMessage,
		] );
	}

	private function validate( $list, $subject, &$sender, $template, $successPage ): void {
		$this->validateNonce();
		$addresses = mailparse_rfc822_parse_addresses( $sender );
		if ( count( $addresses ) > 1 ) {
			$this->addError( 'too-many-senders', $sender );
		} else {
			$address   = $addresses[0];
			$name      = $address['display'];
			$email     = $address['address'];
			$validator = new EmailValidator();
			if ( ! $validator->isValid( $email, new RFCValidation() ) ) {
				$this->addError( 'invalid-sender-address', $email );
			} else if ( $name == $email ) {
				$sender = $email;
			} else {
				$sender = $name . ' <' . $email . '>';
			}
		}
		try {
			( new Templates( MailgunPlugin::instance()->getHttpClient(),
				MailgunPlugin::instance()->getRequestBuilder(),
				MailgunPlugin::instance()->getHydrator() ) )->show( MailgunSettings::getMailgunDomain(), $template );
		} catch ( \Exception $e ) {
			$this->addError( 'template-not-found', $template );
		}
		if ( $successPage && ( get_post_type( $successPage ) != 'page' ) ) {
			$this->addError( 'page-not-found', $successPage );
		}
	}

}