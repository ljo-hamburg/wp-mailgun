<?php

namespace Codello\WPMailgun;

use Codello\WP\AjaxAction;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Egulias\EmailValidator\Validation\MultipleValidationWithAnd;
use Egulias\EmailValidator\Validation\NoRFCWarningsValidation;

class SubscriptionRequestAction extends AjaxAction {

	public function __construct() {
		parent::__construct( 'wp-mailgun-request-subscribe' );
	}

	public function perform(): void {
		$list        = $this->getParameter( "list", true );
		$address     = $this->getParameter( "address", true );
		$name        = $this->getParameter( "name", true );
		$mailingList = MailgunSettings::getMailingListSettings( $list );
		$this->validate( $name, $address, $list, $mailingList );
		$this->checkForErrors();
		$options = [
			'from'                        => $mailingList->sender,
			'to'                          => $address,
			'subject'                     => $mailingList->subject,
			'template'                    => $mailingList->template,
			'v:newsletter_subscribe_link' => MailgunPlugin::instance()->getSubscribeLink( $name, $address, $list ),
			't:text'                      => 'yes',
		];
		if ( isset( $name ) ) {
			$options['v:name'] = $name;
		}
		MailgunPlugin::mailgun()->messages()->send( MailgunSettings::getMailgunDomain(), $options );
		$this->done();
	}

	private function validate( $name, $address, $list, MailingListSettings $mailingList ) {
		$this->validateNonce();
		$validator = new EmailValidator();
		$valid     = $validator->isValid( $address, new MultipleValidationWithAnd( [
			new NoRFCWarningsValidation(),
			new DNSCheckValidation(),
		] ) );
		if ( ! $valid ) {
			$this->addError( 'invalid-address', $address );
		}
		try {
			MailgunPlugin::mailgun()->mailingList()->show( $list );
		} catch ( \Exception $e ) {
			$this->addError( 'list-not-found', $list );
		}
		if ( ! $mailingList->isValid() ) {
			$this->addError( 'list-not-configured', $list );
		}
	}
}