<?php

namespace Codello\WPMailgun;

require_once __DIR__ . '/../mailgun/MailingListMember.php';

use Codello\WP\AjaxAction;
use Mailgun\Custom\MailingListMember;

class SubscribeAction extends AjaxAction {

	public function __construct() {
		parent::__construct( 'wp-mailgun-subscribe' );
	}

	public function perform(): void {
		$name    = $this->getParameter( 'name' );
		$list    = $this->getParameter( 'list', true );
		$address = $this->getParameter( 'address', true );
		$key     = $this->getParameter( 'key', true );
		$this->validate( $name, $address, $list, $key );
		try {
			$mailgunPlugin = MailgunPlugin::instance();
			( new MailingListMember( $mailgunPlugin->getHttpClient(),
				$mailgunPlugin->getRequestBuilder(),
				$mailgunPlugin->getHydrator() ) )
				->create( $list, $address, $name, [], true, true );
			$redirect = MailgunSettings::getMailingListSettings( $list )->getSuccessURL();
			if ( $redirect ) {
				wp_redirect( $redirect );
				exit;
			} else {
				wp_die( "Success" . $redirect );
			}
		} catch ( \Exception $e ) {
			var_dump( $e );
			wp_die( "could not subscribe. Please try again later", 500 );
		}
	}

	public function validate( $name, $address, $list, $key ) {
		$this->validateRequiredParameters( 'list', 'address', 'name', 'name', 'key' );
		if ( ! MailgunPlugin::instance()->validateConfirmationHash( $name, $address, $list, $key ) ) {
			# TODO: Possibly use an error page here?
			wp_die( "authorization key is incorrect", 403 );
		}
		// The email address has already been validated when the confirmation hash was generated.
	}

}