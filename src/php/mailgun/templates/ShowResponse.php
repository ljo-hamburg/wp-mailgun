<?php

namespace Mailgun\Custom;

require_once 'Template.php';

use Mailgun\Model\ApiResponse;

final class ShowResponse implements ApiResponse {

	private $template;

	public static function create( array $data ): self {
		$model           = new self();
		$model->template = Template::create( $data['template'] );

		return $model;
	}

	private function __construct() {
	}

	public function getTemplate(): Template {
		return $this->template;
	}
}