<?php

namespace Mailgun\Custom;

use DateTimeImmutable;

final class Template implements \Mailgun\Model\ApiResponse {

	private ?string $id;
	private ?string $name;
	private ?string $description;
	private ?DateTimeImmutable $createdAt;

	public static function create( array $data ): self {
		$model              = new self();
		$model->id          = $data['id'] ?? null;
		$model->name        = $data['name'] ?? null;
		$model->description = $data['description'] ?? null;
		$model->createdAt   = isset( $data['createdAt'] ) ? new DateTimeImmutable( $data['createdAt'] ) : null;

		return $model;
	}

	private function __construct() {
	}

	public function getId(): ?string {
		return $this->id;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function getDescription(): ?string {
		return $this->description;
	}

	public function getCreatedAt(): ?DateTimeImmutable {
		return $this->createdAt;
	}

}