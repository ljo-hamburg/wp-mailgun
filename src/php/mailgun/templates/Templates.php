<?php

namespace Mailgun\Custom;

require_once "ShowResponse.php";

use Mailgun\Api\HttpApi;
use Mailgun\Assert;

class Templates extends HttpApi {
	// TODO: Implement other methods as well

	/**
	 * Returns a single template.
	 *
	 * @param string $domain  The domain the template belongs to.
	 * @param string $name    The name of the template.
	 * @param bool   $archive If this flag is set to true the active version of the template is included into a
	 *                        response.
	 *
	 * @return ShowResponse
	 * @throws \Exception
	 */
	public function show( string $domain, string $name, bool $archive = false ) {
		Assert::stringNotEmpty( $domain );
		Assert::string( $name );

		$response = $this->httpGet( sprintf( '/v3/%s/templates/%s', $domain, $name ) );

		return $this->hydrateResponse( $response, ShowResponse::class );
	}
}