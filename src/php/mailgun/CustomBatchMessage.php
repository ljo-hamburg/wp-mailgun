<?php

namespace Mailgun\Custom;

use Mailgun\Message\BatchMessage;
use Mailgun\Message\Exceptions\MissingRequiredParameter;
use Mailgun\Message\Exceptions\RuntimeException;

// TODO: Submit a fix to GitHub
class CustomBatchMessage extends BatchMessage {
	/**
	 * @throws RuntimeException
	 * @throws MissingRequiredParameter
	 */
	public function finalize(): void {
		$message = $this->message;

		$domainProperty = new \ReflectionProperty( parent::class, 'domain' );
		$domainProperty->setAccessible( true );
		$domain = $domainProperty->getValue( $this );

		// if ( empty( $this->domain ) ) {
		if ( empty( $domain ) ) {
			throw new RuntimeException( 'You must call BatchMessage::setDomain before sending messages.' );
		}

		if ( empty( $message['from'] ) ) {
			throw MissingRequiredParameter::create( 'from' );
		}

		if ( empty( $message['to'] ) ) {
			throw MissingRequiredParameter::create( 'to' );
		}

		if ( empty( $message['subject'] ) ) {
			throw MissingRequiredParameter::create( 'subject' );
		}

		$batchRecipientAttributesProperty = new \ReflectionProperty( parent::class, 'batchRecipientAttributes' );
		$batchRecipientAttributesProperty->setAccessible( true );
		$batchRecipientAttributes = $batchRecipientAttributesProperty->getValue( $this );

		$apiProperty = new \ReflectionProperty( parent::class, 'api' );
		$apiProperty->setAccessible( true );
		$api = $apiProperty->getValue( $this );

		// $message['recipient-variables'] = json_encode( $this->batchRecipientAttributes );
		$message['recipient-variables'] = json_encode( $batchRecipientAttributes );
		$response                       = $api->send( $domain, $message );
		// $response                       = $this->api->send( $this->domain, $message );

		// $this->batchRecipientAttributes      = [];
		$batchRecipientAttributesProperty->setValue( $this, [] );
		$this->counters['recipients']['to']  = 0;
		$this->counters['recipients']['cc']  = 0;
		$this->counters['recipients']['bcc'] = 0;
		unset( $this->message['to'] );

		$messageIdsProperty = new \ReflectionProperty( parent::class, 'messageIds' );
		$messageIdsProperty->setAccessible( true );
		$messageIdsProperty->setValue( $this, $response->getId() );
		// $this->messageIds[] = $response->getId();
	}
}