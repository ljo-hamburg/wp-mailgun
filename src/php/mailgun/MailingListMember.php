<?php

namespace Mailgun\Custom;

use Mailgun\Assert;
use Mailgun\Api\MailingList\Member;
use Mailgun\Model\MailingList\Member\CreateResponse;

// TODO: Submit a fix to GitHub
class MailingListMember extends Member {
	/**
	 * Creates (or updates) a member of the mailing list.
	 *
	 * @param string $list       Address of the mailing list
	 * @param string $address    Address for the member
	 * @param string $name       Name for the member (optional)
	 * @param array  $vars       Array of field => value pairs to store additional data
	 * @param bool   $subscribed `true` to add as subscribed (default), `false` as unsubscribed
	 * @param bool   $upsert     `true` to update member if present, `false` to raise error in case of a duplicate
	 *                           member (default)
	 *
	 * @return CreateResponse
	 *
	 * @throws \Exception
	 */
	public function create( string $list, string $address, string $name = null, array $vars = [], bool $subscribed = true, bool $upsert = false ) {
		Assert::stringNotEmpty( $list );
		Assert::stringNotEmpty( $address );
		Assert::nullOrStringNotEmpty( $name );

		$params = [
			'address'    => $address,
			'vars'       => \json_encode( $vars ),
			'subscribed' => $subscribed ? 'yes' : 'no',
			'upsert'     => $upsert ? 'yes' : 'no',
		];
		if ( $name ) {
			$params['name'] = $name;
		}

		$response = $this->httpPost( sprintf( '/v3/lists/%s/members', $list ), $params );

		return $this->hydrateResponse( $response, CreateResponse::class );
	}
}