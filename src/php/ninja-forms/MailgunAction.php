<?php

namespace Codello\WPMailgun;

use NF_Abstracts_Action;

/**
 * Class MailgunAction
 * This class adds a 'Mailgun' action to Ninja Forms. The action can be configured with a template name. If triggered
 * it requests the specified template to be used to send a mail to some recipients. The action can be configured
 * similarly to the default email action (from, to, cc, bcc, ...).
 *
 * The form data will be transmitted to Mailgun for template processing. That way data entered in the form can be used
 * in template processing in order to send personalized mails.
 */
final class MailgunAction extends NF_Abstracts_Action {
	protected $_name = 'mailgun-ninja-forms';
	protected $_tags = [ 'mail', 'mailgun', 'forms', 'template' ];
	protected $_timing = 'late';

	/**
	 * MailgunAction constructor. Registers the MailgunAction.
	 */
	public function __construct() {
		parent::__construct();
		$this->_nicename = __( 'Mailgun Template', 'ninja-forms-mailgun' );
		$settings        = include dirname( __FILE__ ) . '/MailgunActionConfig.php';
		$this->_settings = array_merge( $this->_settings, $settings );
	}

	/**
	 * Processes the action using data from the form. This method validates all settings and sends an email via a
	 * Mailgun template.
	 *
	 * @param $action_settings array The settings for this action
	 * @param $form_id         string ID of the form that fire this action.
	 * @param $data            array Form data entered by the user.
	 *
	 * @return mixed <code>$data</code>.
	 */
	public function process( $action_settings, $form_id, $data ) {
		// Convert the action's settings (receiver, sender, cc, ...) into a canonical form for further processing.
		$action_settings = $this->sanitize_address_fields( $action_settings );
		// Check for any configuration errors
		$errors = $this->check_for_errors( $action_settings );

		if ( ! $errors ) {
			// Prepare the mailgun call using the action's configuration
			$json_data = $this->get_json_data( $action_settings, $data );
			$domain    = MailgunSettings::getMailgunDomain();
			$mg        = MailgunPlugin::mailgun();
			try {
				$args   = [
					'from'                  => $this->_format_from( $action_settings ),
					'template'              => $action_settings['mailgun_template'],
					'h:X-Mailgun-Variables' => $json_data,
					't:text'                => 'yes',
				];
				$fields = [ 'to', 'cc', 'bcc' ];
				foreach ( $fields as $key ) {
					if ( isset( $action_settings[ $key ] ) ) {
						$args[ $key ] = $action_settings[ $key ];
					}
				}
				if ( isset( $action_settings['email_subject'] ) ) {
					$args['subject'] = $action_settings['email_subject'];
				}
				if ( isset( $action_settings['reply-to'] ) ) {
					$args['h:Reply-To'] = $action_settings['reply-to'];
				}
				// Send the message
				$mg->messages()->send( $domain, array_filter( $args ) )->getMessage();
			} catch ( \Exception $e ) {
				// Display debug info for users with the correct permissions
				if ( current_user_can( 'manage_options' ) ) {
					$errors['send_error'] = sprintf( __( 'There was an error sending the data. The error message is: %s' ), $e->getMessage() );
				} else {
					$errors['send_error'] = __( 'There was an error sending the data. Please try again later' );
				}
			}
		}

		// Return any potential errors to display to the user
		if ( $errors ) {
			$data['errors']['form'] = $errors;
		}
		if ( current_user_can( 'manage_options' ) && is_user_logged_in() ) {
			$data['actions']['email']['to'] = $action_settings['to'];
		}

		return $data;
	}

	/**
	 * Convert the action's settings into a canonical format for further processing. This means that all email fields
	 * are converted into arrays of email addresses.
	 *
	 * @param $action_settings array The action's configuration
	 *
	 * @return mixed <code>$action_settings</code>
	 */
	protected function sanitize_address_fields( $action_settings ) {
		$email_address_settings = [ 'to', 'from_address', 'reply_to', 'cc', 'bcc' ];
		foreach ( $email_address_settings as $setting ) {
			if ( ! isset( $action_settings[ $setting ] ) || ! $action_settings[ $setting ] ) {
				continue;
			}

			$sanitized_array = [];
			if ( is_array( $action_settings[ $setting ] ) ) {
				$email_addresses = $action_settings[ $setting ];
			} else {
				$email_addresses = explode( ',', $action_settings[ $setting ] );
			}

			foreach ( $email_addresses as $email ) {
				$email = trim( $email );
				if ( empty( $email ) ) {
					continue;
				}
				$sanitized_array[] = $email;
			}
			$action_settings[ $setting ] = $sanitized_array;
		}

		return $action_settings;
	}

	/**
	 * Validates the action's settings for any potential errors. This includes validating email addresses and JSON
	 * formatting.
	 *
	 * @param $action_settings array The action's configuration.
	 *
	 * @return array An associative array of errors.
	 */
	protected function check_for_errors( $action_settings ) {
		$errors = [];
		if ( ! current_user_can( 'manage_options' ) ) {
			return $errors;
		}

		// Plugin wide errors
		if ( ! MailgunSettings::validate() ) {
			$errors['invalid-settings'] = __( "The Mailgun Plugin is not correctly configured.", "wp-mailgun" );
		}

		// Local Errors
		$email_address_settings = [ 'to', 'from_address', 'reply_to', 'cc', 'bcc' ];
		foreach ( $email_address_settings as $setting ) {
			if ( ! isset( $action_settings[ $setting ] ) || ! $action_settings[ $setting ] ) {
				continue;
			}

			$email_addresses = is_array( $action_settings[ $setting ] ) ? $action_settings[ $setting ] : explode( ',', $action_settings[ $setting ] );

			foreach ( (array) $email_addresses as $email ) {
				$email = trim( $email );
				if ( preg_match( '/<([^>]*)>$/', $email, $email ) ) {
					$email = $email[1];
				}
				if ( ! is_email( $email ) ) {
					$errors['invalid_email'] = sprintf( __( 'Your email action "%s" has an invalid value for the "%s" setting. Please check this setting and try again.', 'ninja-forms' ), $action_settings['label'], $setting );
				}
			}
		}
		if ( ! isset( $action_settings['mailgun_template'] ) || ! $action_settings['mailgun_template'] ) {
			$errors['no_template'] = sprintf( __( 'Your Mailgun action "%s" is missing a template name. Please check this setting and try again.', 'ninja-forms-mailgun' ), $action_settings['label'] );
		}
		if ( isset( $action_settings['custom_json'] ) && ! json_decode( $action_settings['custom_json'] ) ) {
			$errors['invalid_json'] = sprintf( __( 'Your Mailgun action "%s" contains invalid JSON data. Please check this setting and try again.', 'ninja-forms-mailgun' ), $action_settings['label'] );
		}

		return $errors;
	}

	/**
	 * Returns a formatted version of the "From" field. This function uses the action's configuration or - as a fallback
	 * - the wordpress blog name and admin email address.
	 *
	 * @param $settings array The action's configuration
	 *
	 * @return string A formatted email address
	 */
	private function _format_from( $settings ) {
		$from_name = get_bloginfo( 'name', 'raw' );
		$from_name = apply_filters( 'ninja_forms_action_mailgun_from_name', $from_name );
		$from_name = ( $settings['from_name'] ) ? $settings['from_name'] : $from_name;

		$from_address = get_bloginfo( 'admin_email' );
		$from_address = apply_filters( 'ninja_forms_action_mailgun_from_address', $from_address );
		$from_address = ( $settings['from_address'] ) ? $settings['from_address'] : $from_address;

		if ( $from_name ) {
			return "$from_name <$from_address>";
		} else {
			return $from_address;
		}
	}

	/**
	 * Returns the JSON data to be sent to Mailgun for template processing. The returned data includes any custom JSON
	 * data as well as key value pairs for any form fields. The keys for a form field are its unique id as well as its
	 * custom name (if it has been set).
	 *
	 * @param $action_settings array The action's configuration.
	 * @param $data            array The form data.
	 *
	 * @return string A JSON formatted string to be sent to Mailgun.
	 */
	protected function get_json_data( $action_settings, $data ) {
		$json = [
			"form-title" => $data['settings']['title'],
		];
		foreach ( $data['fields'] as $id => $field ) {
			$settings                 = $field['settings'];
			$json[ $settings['key'] ] = $settings['value'];
			if ( isset( $settings['custom_name_attribute'] ) && $settings['custom_name_attribute'] ) {
				$json[ $settings['custom_name_attribute'] ] = $settings['value'];
			}
		}
		$custom = json_decode( $action_settings['custom_json'], true );
		if ( $custom ) {
			$json = array_merge( $json, $custom );
		}

		return json_encode( $json );
	}
}
