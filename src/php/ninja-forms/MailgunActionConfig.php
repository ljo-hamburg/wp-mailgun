<?php

namespace Codello\WPMailgun;

/**
 * This file returns the Ninja Forms configuration fo the Mailgun action.
 */
return [
	/**
	 * Mail Recipients
	 */
	'to'               => [
		'name'           => 'to',
		'type'           => 'textbox',
		'group'          => 'primary',
		'label'          => __( 'To', 'ninja-forms' ),
		'placeholder'    => __( 'Email address or search for a field', 'ninja-forms' ),
		'value'          => '{wp:admin_email}',
		'width'          => 'one-half',
		'use_merge_tags' => true,
	],

	/**
	 * Reply-To Header
	 */
	'reply_to'         => [
		'name'           => 'reply_to',
		'type'           => 'textbox',
		'group'          => 'primary',
		'label'          => __( 'Reply To', 'ninja-forms' ),
		'placeholder'    => '',
		'value'          => '',
		'width'          => 'one-half',
		'use_merge_tags' => true,
	],

	/**
	 * Subject of the E-Mail
	 */
	'email_subject'    => [
		'name'           => 'email_subject',
		'type'           => 'textbox',
		'group'          => 'primary',
		'label'          => __( 'Subject', 'ninja-forms' ),
		'placeholder'    => __( 'Subject Text or seach for a field', 'ninja-forms' ),
		'value'          => __( 'Ninja Forms Submission', 'ninja-forms' ),
		'width'          => 'full',
		'use_merge_tags' => true,
	],

	/**
	 * Name of the Mailgun template
	 */
	'mailgun_template' => [
		'name'           => 'mailgun_template',
		'type'           => 'textbox',
		'group'          => 'primary',
		'label'          => __( 'Template Name', 'ninja-forms-mailgun' ),
		'placeholder'    => __( 'Name of the Mailgun template', 'ninja-forms-mailgun' ),
		'value'          => '',
		'width'          => 'full',
		'use_merge_tags' => false,
	],

	/**
	 * Name of the sender
	 */
	'from_name'        => [
		'name'           => 'from_name',
		'type'           => 'textbox',
		'group'          => 'advanced',
		'label'          => __( 'From Name', 'ninja-forms' ),
		'placeholder'    => __( 'Name or fields', 'ninja-forms' ),
		'value'          => '',
		'width'          => 'one-half',
		'use_merge_tags' => true,
	],

	/**
	 * Address of the sender
	 */
	'from_address'     => [
		'name'           => 'from_address',
		'type'           => 'textbox',
		'group'          => 'advanced',
		'label'          => __( 'From Address', 'ninja-forms' ),
		'placeholder'    => __( 'One email address or field', 'ninja-forms' ),
		'value'          => '',
		'use_merge_tags' => true,
	],

	/**
	 * CC Addresses
	 */
	'cc'               => [
		'name'           => 'cc',
		'type'           => 'textbox',
		'group'          => 'advanced',
		'label'          => __( 'Cc', 'ninja-forms' ),
		'placeholder'    => '',
		'value'          => '',
		'use_merge_tags' => true,
	],

	/**
	 * BCC Addresses
	 */
	'bcc'              => [
		'name'           => 'bcc',
		'type'           => 'textbox',
		'group'          => 'advanced',
		'label'          => __( 'Bcc', 'ninja-forms' ),
		'placeholder'    => '',
		'value'          => '',
		'use_merge_tags' => true,
	],

	/**
	 * Custom JSON data to be available for template processing.
	 */
	'custom_json'      => [
		'name'           => 'custom_json',
		'type'           => 'textarea',
		'group'          => 'advanced',
		'label'          => __( 'Custom JSON Data', 'ninja-forms-mailgun' ),
		'placeholder'    => __( 'Add custom data for the Mailgun template in JSON format' ),
		'value'          => '{}',
		'width'          => 'full',
		'use_merge_tags' => true,
	],
];