<?php

namespace Codello\WPMailgun;

use Codello\WP\WPScript;
use Codello\WP\WPShortcode;

// TODO: Allow to subscribe to more than one list at a time
class MailgunShortcode extends WPShortcode {

	private WPScript $script;

	public function __construct() {
		parent::__construct( 'mailgun' );
		$this->script = new WPScript( 'wp-mailgun-list-subscribe',
			plugins_url( 'assets/list-subscribe.js', MailgunPlugin::instance()->getFile() ),
			[ 'jquery' ],
			null, [
				'footer'      => true,
				'autoEnqueue' => false,
			] );
	}

	public function register() {
		parent::register();
		$this->script->register();
	}

	public function render( $atts = [], $content = null, $tag = '' ) {
		$mg_atts = shortcode_atts( [
			'list' => null,
		], $atts, $tag );

		$list = $mg_atts['list'];
		if ( ! $list ) {
			ob_start() ?>
            <span><strong>Configuration Error:</strong> You must specify a <code>list</code> to render this form!</span>
			<?php return ob_get_clean();
		}
		ob_start();
		$this->renderShortcode( $list, $content );

		return ob_get_clean();
	}

	private function renderShortcode( $list, $content ) {
		$mailingList = MailgunSettings::getMailingListSettings( $list );
		$this->script->enqueueScript() ?>
        <div class="mailgun-shortcode">
			<?php if ( $content )
				echo $content ?>
            <div class="success message"><?= esc_html( $mailingList->successMessage ) ?></div>
            <div class="error message"><?= esc_html( $mailingList->errorMessage ) ?></div>
            <form action="<?= admin_url( 'admin-ajax.php' ) ?>">
                <input type="hidden" name="action" value="wp-mailgun-request-subscribe">
                <input type="hidden" name="list" value="<?= esc_attr( $list ) ?>">
				<?php wp_nonce_field( 'wp-mailgun-request-subscribe' ) ?>
                <fieldset class="subscribe-form">
                    <div class="has-float-label">
                        <input type="text" id="name" name="name" required
                               placeholder="<?= esc_html__( 'Name', 'wp-mailgun' ) ?>">
                        <label for="name"><?= esc_html__( 'Name', 'wp-mailgun' ) ?></label>
                    </div>
                    <div class="has-float-label">
                        <input type="email" id="address" name="address" required
                               placeholder="<?= esc_html__( 'name@example.com', 'wp-mailgun' ) ?>">
                        <label for="address"><?= esc_html__( 'E-Mail', 'wp-mailgun' ) ?></label>
                    </div>
                    <button type="submit"
                            class="buttonload lc_button"
                            data-loading-text="<?= esc_attr__( 'Processing…', 'wp-mailgun' ) ?>">
						<?= esc_html__( 'Subscribe', 'wp-mailgun' ) ?>
                    </button>
                </fieldset>
            </form>
        </div>
	<?php }
}