import jQuery from 'jquery';

(($ => {
    $(document).ready(() => {
        $('.mailgun-shortcode').each((index, element) => {
            const form = $(element).children("form");
            const button = $(element).find('button')[0];
            const buttonLoadingText = button.dataset.loadingText;
            const buttonText = button.innerHTML;
            const successMessage = $(element).find('.success.message');
            const errorMessage = $(element).find('.error.message');
            element.onsubmit = event => {
                event.preventDefault();
                successMessage.removeClass('visible');
                errorMessage.removeClass('visible');
                button.innerHTML = '<i class="fa fa-circle-o-notch fa-spin"></i> ' + buttonLoadingText;
                button.disabled = true;
                $.ajax({
                    type: 'GET',
                    url: form.attr("action"),
                    data: form.serialize()
                }).done((data, textStatus, jqXHR) => {
                    form.remove();
                    successMessage.addClass('visible');
                }).fail((data, textStatus, jqXHR) => {
                    button.innerHTML = buttonText;
                    button.disabled = false;
                    errorMessage.addClass('visible');
                });
                return false;
            };
        });
    });
})(jQuery));