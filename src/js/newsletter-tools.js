import jQuery from 'jquery';
import 'jquery-modal';
import {__} from '@wordpress/i18n';
import {clearAdminMessages, showAdminMessage} from "../../../Wordpress Helpers/src/js/wp-admin";

require('vendor/codello/wp-helpers/src/js/dependencies');

(($ => {
    $(document).ready(() => {
        const $editForm = $('#mailing-list-edit-form');
        const $errorElement = $editForm.find('.error-message');
        const $submitButton = $editForm.find('button.submit');
        const submitButtonText = $submitButton.html();
        const $heading = $editForm.children('.name');
        const $list = $editForm.find('input[name=list]');
        const $subject = $editForm.find('input[name=subject]');
        const $sender = $editForm.find('input[name=sender]');
        const $template = $editForm.find('input[name=template]');
        const $successPage = $editForm.find('select[name=successPage]');
        const $successMessage = $editForm.find('textarea[name=successMessage]');
        const $errorMessage = $editForm.find('textarea[name=errorMessage]');

        $editForm.on('submit', event => {
            event.preventDefault();
            $errorElement.text('');
            $submitButton.attr("disabled", true);
            $submitButton.html('<i class="fa fa-circle-o-notch fa-spin"></i> ' + submitButtonText);
            $.getJSON({
                type: "POST",
                url: $editForm.attr('action'),
                data: $editForm.serialize()
            }).done((data) => {
                $.modal.close();
                const element = $(`.mg-mailing-list[data-list-address="${data.list}"]`)[0];
                element.dataset.subject = data.subject;
                element.dataset.sender = data.sender;
                element.dataset.template = data.template;
                element.dataset.successPage = data.successPage;
                element.dataset.successMessage = data.successMessage;
                element.dataset.errorMessage = data.errorMessage;
                const $dot = $(element).find('.dot');
                $dot.removeClass('red');
                $dot.add('green');
                showAdminMessage(__("Settings Saved", "wp-mailgun"), 'notice-success')
            }).fail((jqXHR) => {
                const errors = jqXHR.responseJSON.errors;
                if ('missing-parameter' in errors) {
                    $errorElement.text(__("Not all required fields have been filled.", "wp-mailgun"));
                } else if ('too-many-senders' in errors) {
                    $errorElement.text(__('You can only specify one sender.', 'wp-mailgun'));
                } else if ('invalid-sender-address' in errors) {
                    $errorElement.text(__('The sender address is invalid.', 'wp-mailgun'));
                } else if ('template-not-found' in errors) {
                    $errorElement.text(__('The template could not be found.', 'wp-mailgun'));
                } else if ('page-not-found' in errors) {
                    $errorElement.text(__('The Success Page could not be found.', 'wp-mailgun'));
                } else {
                    $errorElement.text(__('An unknown error occurred', 'wp-mailgun'));
                }
            }).always(() => {
                $submitButton.attr("disabled", false);
                $submitButton.html(submitButtonText);
            });
        });
        $('.mg-mailing-list').each((index, element) => {
            $(element).find('.configure').click(() => {
                $errorElement.text('');
                $heading.text(element.dataset.listName);
                $list.val(element.dataset.listAddress);
                $subject.val(element.dataset.subject);
                $sender.val(element.dataset.sender);
                $template.val(element.dataset.template);
                $successPage.val(element.dataset.successPage);
                $successMessage.val(element.dataset.successMessage);
                $errorMessage.val(element.dataset.errorMessage);
                $editForm.modal({
                    clickClose: false,
                    showClose: false,
                    fadeDuration: 300,
                });
            });
        });

        $('form.newsletter-invite').each((index, form) => {
            const $form = $(form);
            const submitButton = $form.children("button[type=submit]")[0];
            const submitButtonText = submitButton.innerHTML;
            form.addEventListener('submit', event => {
                event.preventDefault();
                clearAdminMessages();
                submitButton.innerHTML = '<i class="fa fa-circle-o-notch fa-spin"></i> ' + submitButtonText;
                submitButton.disabled = true;
                $.getJSON({
                    type: 'POST',
                    url: form.getAttribute('action'),
                    data: $form.serialize()
                }).done((data, textStatus, jqXHR) => {
                    form.reset();
                    showAdminMessage(__('Invitations successfully sent.', 'wp-mailgun'), 'notice-success');
                }).fail((jqXHR, textStatus, errorThrown) => {
                    const errors = jqXHR.responseJSON.errors;
                    let knownError = false;
                    if ('missing-parameter' in errors) {
                        showAdminMessage(__('All fields are required. Please fill out all fields and try again.', 'wp-mailgun'));
                        knownError = true;
                    }
                    if ('template-not-found' in errors) {
                        showAdminMessage(__('The specified template could not be found.', 'wp-mailgun'));
                        knownError = true;
                    }
                    if ('invalid-email' in errors) {
                        let addressString = errors['invalid-email'];
                        if (Array.isArray(addressString)) {
                            addressString = addressString.join(', ');
                        }
                        showAdminMessage(sprintf(__('The following E-Mail addresses are not valid: %s', 'wp-mailgun'), addressString));
                        knownError = true;
                    }
                    if ('no-new-addresses' in errors) {
                        showAdminMessage(__('Your list of email addresses does not contain any new addresses', 'wp-admin'));
                        knownError = true;
                    }
                    if (!knownError) {
                        showAdminMessage(__('An unknown error occurred.', 'wp-mailgun'));
                    }
                }).always((data, textStatus, jqXHR) => {
                    submitButton.innerHTML = submitButtonText;
                    submitButton.disabled = false;
                });
            });
        });
    });
})(jQuery));