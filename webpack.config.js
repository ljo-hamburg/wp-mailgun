const path = require('path');


module.exports = {
    mode: 'production',
    entry: {
        "list-subscribe": './src/js/list-subscribe.js',
        "newsletter-tools": ["./src/js/newsletter-tools.js", "./src/scss/newsletter-tools.scss"],
    },
    output: {
        path: path.resolve(__dirname, 'build/assets'),
        filename: '[name].js'
    },
    resolve: {
        alias: {
            vendor: path.resolve(__dirname, 'vendor/'),
        }
    },
    externals: {
        jquery: 'jQuery',
        '@wordpress/i18n': 'wp.i18n'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ['@wordpress/default'],
                    }
                }
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].css'
                        }
                    },
                    'extract-loader',
                    "css-loader",
                    "postcss-loader",
                    {
                        loader: "sass-loader",
                        options: {
                            sassOptions: {
                                // outputStyle: 'expanded'
                                outputStyle: 'compressed'
                            }
                        }
                    }
                ]
            }
        ]
    }
};