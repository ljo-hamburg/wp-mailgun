msgid ""
msgstr ""
"Project-Id-Version: Mailgun for WordPress 1.0.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/build\n"
"POT-Creation-Date: 2020-03-07T20:06:29+00:00\n"
"PO-Revision-Date: 2020-03-06 11:53+0100\n"
"Last-Translator: Kim Wittenburg <codello@wittenburg.kim>\n"
"Language-Team: \n"
"Language: de_DE\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.3\n"
"X-Domain: wp-mailgun\n"

#. Plugin Name of the plugin
msgid "Mailgun for WordPress"
msgstr "Mailgun for WordPress"

#. Plugin URI of the plugin
msgid "https://gitlab.com/ljo-hamburg/wp-mailgun"
msgstr ""

#. Description of the plugin
msgid "A WordPress plugin for Mailgun."
msgstr "Ein WordPress-Plugin für Mailgun."

#. Author of the plugin
msgid "Kim Wittenburg"
msgstr "Kim Wittenburg"

#. Author URI of the plugin
msgid "https://ljo-hamburg.de"
msgstr ""

#: actions/MailgunSecretAction.php:13 classes/MailgunSecretAction.php:13
msgid "Pending subscriptions invalidated."
msgstr ""

#: admin-pages/DomainSettingsField.php:10
#: admin-pages/MailingListSettingsField.php:10
#: classes/DomainSettingsField.php:10 classes/MailingListSettingsField.php:10
msgid "Please Choose"
msgstr "Bitte auswählen…"

#: admin-pages/DomainSettingsField.php:18 classes/DomainSettingsField.php:18
msgid "Enter your API key and save to select a domain."
msgstr "Gib zuerst einen API-Key an und klicke auf speichern."

#: admin-pages/DomainSettingsField.php:42 classes/DomainSettingsField.php:42
msgid "The Mailgun domain was not found."
msgstr "Die angegebene Mailgun-Domain wurde nicht gefunden."

#: admin-pages/MailingListSettingsField.php:19
#: classes/MailingListSettingsField.php:19
msgid "Enter your API key and save to select a mailing list."
msgstr ""

#: admin-pages/MailingListSettingsField.php:21
#: classes/MailingListSettingsField.php:21
msgid "You don't have any mailing lists yet."
msgstr ""

#: admin-pages/MailingListSettingsField.php:32
#: classes/MailingListSettingsField.php:32
msgid "%s (%d Members)"
msgstr ""

#: admin-pages/MailingListSettingsField.php:43
#: classes/MailingListSettingsField.php:43
msgid "The specified mailing list does not exist"
msgstr ""

#: admin-pages/NewsletterPage.php:66
msgid ""
"The tools on this page allow you to configure advanced settings for your "
"newsletters."
msgstr ""

#: admin-pages/NewsletterPage.php:76
msgid "Could not list your mailing lists. Check if your API key is valid."
msgstr ""

#: admin-pages/NewsletterPage.php:84
#, fuzzy
msgid "Customize Mailing Lists"
msgstr "Mailingliste"

#: admin-pages/NewsletterPage.php:85
msgid "Here you can customize the subscription forms for your mailing lists."
msgstr ""

#: admin-pages/NewsletterPage.php:91
msgid ""
"Configure the settings for this mailing list. This does not affect any other "
"mailing lists."
msgstr ""

#: admin-pages/NewsletterPage.php:97 admin-pages/NewsletterPage.php:200
#: classes/NewsletterPage.php:61
msgid "Subject"
msgstr ""

#: admin-pages/NewsletterPage.php:103
#, fuzzy
msgid "Sender Email"
msgstr "Absender E-Mail-Adresse"

#: admin-pages/NewsletterPage.php:106
msgid "Use the format 'Name <email@example.tld>' to include a sender name."
msgstr ""

#: admin-pages/NewsletterPage.php:110 admin-pages/NewsletterPage.php:195
#: classes/NewsletterPage.php:57
#, fuzzy
msgid "Template"
msgstr "Mailgun-Template"

#: admin-pages/NewsletterPage.php:114
msgid "This template is used for the double opt-in confirmation mail.."
msgstr ""

#: admin-pages/NewsletterPage.php:118
msgid "Successful Subscription Page"
msgstr ""

#: admin-pages/NewsletterPage.php:121
msgid ""
"Select the page to which the user should be redirected after a successful "
"subscription."
msgstr ""

#: admin-pages/NewsletterPage.php:125 admin-pages/NewsletterPage.php:131
msgid "Success Message"
msgstr ""

#: admin-pages/NewsletterPage.php:127
msgid ""
"This message is displayed when a user successfully submitted the subscribe "
"form."
msgstr ""

#: admin-pages/NewsletterPage.php:133
msgid ""
"This message is displayed when a user did enter invalid data in the "
"subscribe form."
msgstr ""

#: admin-pages/NewsletterPage.php:139
msgid "Cancel"
msgstr ""

#: admin-pages/NewsletterPage.php:141
msgid "Save"
msgstr ""

#: admin-pages/NewsletterPage.php:162
msgid "%1$s (%2$s Members)"
msgstr ""

#: admin-pages/NewsletterPage.php:177
msgid "Send Manual Invites"
msgstr ""

#: admin-pages/NewsletterPage.php:178
msgid ""
"Invite users to join your mailing list. Users will receive an email and can "
"click a\n"
"             link in that email to subscribe to your newsletter. You can use "
"{{ newsletter_subscribe_link }} in your\n"
"             email to include that subscribe link."
msgstr ""

#: admin-pages/NewsletterPage.php:187
msgid "Mailing List"
msgstr "Mailingliste"

#: admin-pages/NewsletterPage.php:204 classes/NewsletterPage.php:65
msgid "Addresses"
msgstr ""

#: admin-pages/NewsletterPage.php:210
msgid ""
"Enter the recipients' email addresses in the format\n"
"                         'Name <address@example.tld>'. Multiple addresses "
"can be separated by a comma."
msgstr ""

#: admin-pages/NewsletterPage.php:214 classes/NewsletterPage.php:75
msgid "Invite"
msgstr ""

#: admin-pages/SettingsPage.php:22
msgid "Mailgun Settings"
msgstr "Mailgun-Einstellungen"

#: admin-pages/SettingsPage.php:22
msgid "Mailgun"
msgstr "Mailgun"

#: admin-pages/SettingsPage.php:27
msgid "Mailgun Account"
msgstr "Mailgun-Konto"

#: admin-pages/SettingsPage.php:28
msgid "API Key"
msgstr "API-Key"

#: admin-pages/SettingsPage.php:33
msgid "Server Location"
msgstr "Serverstandort"

#: admin-pages/SettingsPage.php:35
msgid "United States"
msgstr "USA"

#: admin-pages/SettingsPage.php:36
msgid "Europe"
msgstr "Europa"

#: admin-pages/SettingsPage.php:40
msgid "Domain"
msgstr "Domain"

#: admin-pages/SettingsPage.php:41
msgid "Required Role"
msgstr ""

#: admin-pages/SettingsPage.php:43
msgid "Select the role required to edit newsletter forms and invite users."
msgstr ""

#: classes/MailgunPlugin.php:60
#, fuzzy
msgid "Your Mailgun Settings are incomplete"
msgstr "Mailgun-Einstellungen"

#: classes/NewsletterPage.php:45
msgid ""
"The tools on this page allow you to configure advanced settings for your "
"newsletters.\n"
"\t\t\tUse with caution."
msgstr ""

#: classes/ResetSecretButton.php:30
msgid "Invalidate"
msgstr ""

#: ninja-forms/MailgunAction.php:145
#, fuzzy
msgid "The Mailgun Plugin is not correctly configured."
msgstr "Die angegebene Mailgun-Domain wurde nicht gefunden."

#: shortcodes/MailgunShortcode.php:61 shortcodes/MailgunShortcode.php:62
msgid "Name"
msgstr ""

#: shortcodes/MailgunShortcode.php:66
msgid "name@example.com"
msgstr ""

#: shortcodes/MailgunShortcode.php:67
msgid "E-Mail"
msgstr ""

#: shortcodes/MailgunShortcode.php:71
msgid "Processing…"
msgstr ""

#: shortcodes/MailgunShortcode.php:72
msgid "Subscribe"
msgstr ""

#: wp-mailgun.php:39
msgid ""
"This plugin is not completely installed. This issue may occur if you "
"installed the plugin without the \n"
"    required dependencies. Please reinstall or update the plugin."
msgstr ""

#: assets/newsletter-tools.js:1
msgid "Settings Saved"
msgstr ""

#: assets/newsletter-tools.js:1
msgid "Not all required fields have been filled."
msgstr ""

#: assets/newsletter-tools.js:1
msgid "You can only specify one sender."
msgstr ""

#: assets/newsletter-tools.js:1
msgid "The sender address is invalid."
msgstr ""

#: assets/newsletter-tools.js:1
msgid "The template could not be found."
msgstr ""

#: assets/newsletter-tools.js:1
msgid "The Success Page could not be found."
msgstr ""

#: assets/newsletter-tools.js:1
msgid "An unknown error occurred"
msgstr ""

#: assets/newsletter-tools.js:1
msgid "Invitations successfully sent."
msgstr ""

#: assets/newsletter-tools.js:1
msgid "All fields are required. Please fill out all fields and try again."
msgstr ""

#: assets/newsletter-tools.js:1
msgid "The specified template could not be found."
msgstr ""

#: assets/newsletter-tools.js:1
msgid "The following E-Mail addresses are not valid: %s"
msgstr ""

#: assets/newsletter-tools.js:1
msgid "An unknown error occurred."
msgstr ""

#: assets/reset-secret.js:1
msgid ""
"Invalidate all pending subscriptions? Users that have not clicked the "
"confirmation link yet will need to enter their E-Mail address again."
msgstr ""
"Alle offenen Abos verwerfen? Wenn jemand noch nicht den Bestätigungslink in "
"der E-Mail geklickt hat, muss der Newsletter neu abonniert werden."

#: assets/reset-secret.js:1
msgid "Success"
msgstr ""

#~ msgid "Newsletter"
#~ msgstr "Newsletter"
